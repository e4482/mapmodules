<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints a particular instance of mapmodules
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_mapmodules
 * @copyright  2015 Pascal Fautrero pascal.fautrero@ac-versailles.fr
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/// (Replace mapmodules with the name of your module and remove this line)

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');

$id = optional_param('id', 0, PARAM_INT); // course_module ID, or
$n  = optional_param('n', 0, PARAM_INT);  // mapmodules instance ID - it should be named as the first character of the module

if ($id) {
    $cm         = get_coursemodule_from_id('mapmodules', $id, 0, false, MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $mapmodules  = $DB->get_record('mapmodules', array('id' => $cm->instance), '*', MUST_EXIST);
} elseif ($n) {
    $mapmodules  = $DB->get_record('mapmodules', array('id' => $n), '*', MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $mapmodules->course), '*', MUST_EXIST);
    $cm         = get_coursemodule_from_instance('mapmodules', $mapmodules->id, $course->id, false, MUST_EXIST);
} else {
    error('You must specify a course_module ID or an instance ID');
}

require_login($course, true, $cm);
$context = context_module::instance($cm->id);

/// Print the page header

$PAGE->set_url('/mod/mapmodules/view.php', array('id' => $cm->id));
$PAGE->set_title(format_string($mapmodules->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($context);
// do not load maploader here because must be loaded even in section view
//$PAGE->requires->js( new moodle_url($CFG->wwwroot . '/mod/mapmodules/template/maploader.js') );

// other things you may want to set - remove if not needed
//$PAGE->set_cacheable(false);
//$PAGE->set_focuscontrol('some-html-id');
//$PAGE->add_body_class('mapmodules-'.$somevar);

// Output starts here
echo $OUTPUT->header();

if ($mapmodules->intro) { // Conditions to show the intro can change to look for own settings or whatever
    echo $OUTPUT->box(format_module_intro('mapmodules', $mapmodules, $cm->id), 'generalbox mod_introbox', 'mapmodulesintro');
}
else {
    echo get_game_source_code($mapmodules, $context->id);
}

// Finish the page
echo $OUTPUT->footer();
