<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    mod_data
 * @subpackage backup-moodle2
 * @author    2015 Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
 * @author    Nicolas Daugas
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Define all the backup steps that will be used by the backup_data_activity_task
 */

/**
 * Define the complete data structure for backup, with file and id annotations
 */
class backup_mapmodules_activity_structure_step extends backup_activity_structure_step {

    protected function define_structure() {

        // To know if we are including userinfo
        $userinfo = $this->get_setting_value('userinfo');

        // Define each element separated
        $mapmodules = new backup_nested_element('mapmodules', array('id'), array(
            'course', 'name', 'intro', 'introformat', 'timecreated', 'timemodified', 
            'targetsection', 'path', 'iconset', 'displaymodulenames', 'buttonwidth',
            'descriptionheader', 'descriptionheaderformat',
            'descriptionfooter', 'descriptionfooterformat'));


        $mapmodules->set_source_table('mapmodules', array('id' => backup::VAR_ACTIVITYID));

        $mapmodules->annotate_files('mod_mapmodules', 'descriptionheader', null); // This file area hasn't itemid
        $mapmodules->annotate_files('mod_mapmodules', 'descriptionfooter', null); // This file area hasn't itemid
        $mapmodules->annotate_files('mod_mapmodules', 'maps', null); // Take all map images

        // Return the root element (data), wrapped into standard activity structure
        return $this->prepare_activity_structure($mapmodules);
    }
}
