<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package     mod_mapmodules
 * @subpackage  backup-moodle2
 * @author      2015 Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
 * @author      Nicolas Daugas
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Define all the restore steps that will be used by the restore_mapmodules_activity_task
 */

/**
 * Structure step to restore one mapmodules activity
 */
class restore_mapmodules_activity_structure_step extends restore_activity_structure_step {

    protected function define_structure() {

        $paths = array();
        //$userinfo = $this->get_setting_value('userinfo');
        $paths[] = new restore_path_element('mapmodules', '/activity/mapmodules');

        // Return the paths wrapped into standard activity structure
        return $this->prepare_activity_structure($paths);
    }

    protected function process_mapmodules($mapmodules) {
        global $DB;

        $mapmodules = (object)$mapmodules;
        mapmodules_db_upgrade($mapmodules);
        $oldid = $mapmodules->id;

        $mapmodules->course = $this->get_courseid();

        // insert the mapmodules record
        $newitemid = $DB->insert_record('mapmodules', $mapmodules);
        $mapmodules->id = $newitemid;

        //$mapmodules->targetsection = 0;
        $contextid = $this->task->get_contextid();
        $mapmodules->intro = get_game_source_code($mapmodules, $contextid);

        $DB->update_record('mapmodules', $mapmodules);

        $this->apply_activity_instance($newitemid);
    }

    protected function after_execute() {
        global $DB;
        // Add mapmodules related files, no need to match by itemname (just internally handled context)
        $this->add_related_files('mod_mapmodules', 'descriptionheader', null);
        $this->add_related_files('mod_mapmodules', 'descriptionfooter', null);
        $this->add_related_files('mod_mapmodules', 'maps', null);

    }
}
