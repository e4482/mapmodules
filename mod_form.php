<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The main mapmodules configuration form
 *
 * It uses the standard core Moodle formslib. For more info about them, please
 * visit: http://docs.moodle.org/en/Development:lib/formslib.php
 *
 * @package    mod_mapmodules
 * @author     2015 Pascal Fautrero pascal.fautrero@ac-versailles.fr
 * @author     Nicolas Daugas
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/course/moodleform_mod.php');
require_once($CFG->dirroot.'/mod/mapmodules/lib.php');
require_once($CFG->dirroot.'/mod/mapmodules/map_editor.php');

/**
 * Module instance settings form
 */
class mod_mapmodules_mod_form extends moodleform_mod {

    private $themes;

    /**
     * Defines forms elements
     */
    public function definition() {

        global $DB, $CFG, $PAGE;

        // CSS and javascript for map editor
        $PAGE->requires->css(new moodle_url('/mod/mapmodules/css/mapeditor.css'));
        $PAGE->requires->css(new moodle_url('/mod/mapmodules/css/style.css'));
        $PAGE->requires->css(new moodle_url('/mod/mapmodules/css/maps_modal.css'));

        $mform = $this->_form;
        //-------------------------------------------------------------------------------
        // Adding the "general" fieldset, where all the common settings are showed
        $mform->addElement('header', 'general', get_string('general', 'form'));

        // Adding the standard "name" field
        $mform->addElement('html', '<div style="display:none;">');
        $mform->addElement('text', 'name', "Titre : ", array('size'=>'64'));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEAN);
        }
        $mform->addRule('name', get_string('mapvalidation', 'mapmodules'), 'required', null, 'server');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        $mform->addHelpButton('name', 'mapmodulesname', 'mapmodules');
        $mform->addElement('html', '</div>');

        // Map editor
        $contextid = $this->context->id;

        $mapeditor = new map_editor($contextid);
        $mform->addElement('html', $mapeditor->render());

        // Image file
        /*$mform->addElement('button', 'useloadedimage', get_string('useloadedimage', 'mapmodules'), [
            'style' => 'display: none',
            'onclick' => 'useLoadedImage()'
        ]);*/
        $mform->addElement('filepicker', 'mapfile', get_string('mapeditorimagefilelabel', 'mapmodules'), null,
            array('maxbytes' => FILE_AREA_MAX_BYTES_UNLIMITED, 'accepted_types' => array('image')));

        // Hidden fields filled by map edition
        $mform->addElement('hidden', 'path', '');
        $mform->setType('path', PARAM_TEXT);
        $mform->addRule('path', get_string('invalidpathvalidation', 'mapmodules'), 'required', null, 'server');


        // Icon set
        $iconsetsfilecontent = file_get_contents($CFG->dirroot."/mod/mapmodules/res/iconsets.json");
        $iconsets = json_decode($iconsetsfilecontent);
        $options = [];
        foreach ($iconsets as $iconset) {
            $options[$iconset->id] = $iconset->name;
        }

        $mform->addElement('html', "<div id='iconsets' style='display: none'>$iconsetsfilecontent</div>");
        $mform->addElement('select', 'iconset', get_string('iconsetlabel', 'mapmodules'), $options);
        $mform->setType('iconset', PARAM_INT);

        $buttonwidthoptions = [
            0 => get_string('buttonwidthnaught', 'mapmodules'),
            20 => get_string('buttonwidthtiny', 'mapmodules'),
            30 => get_string('buttonwidthverysmall', 'mapmodules'),
            40 => get_string('buttonwidthsmall', 'mapmodules'),
            50 => get_string('buttonwidthmedium', 'mapmodules'),
            70 => get_string('buttonwidthlarge', 'mapmodules'),
            90 => get_string('buttonwidthextralarge', 'mapmodules'),
            120 => get_string('buttonwidthgigantic', 'mapmodules')
        ];
        $mform->addElement('select', 'buttonwidth', get_string('buttonwidthlabel', 'mapmodules'), $buttonwidthoptions);
        $mform->setType('buttonwidth', PARAM_INT);
        $mform->setDefault('buttonwidth', 50);

        $mform->addElement('advcheckbox', 'displaymodulenames', get_string('displaymodulenameslabel', 'mapmodules'), '', null, array(0, 1));
        $mform->setType('displaymodulenames', PARAM_INT);
        $mform->setDefault('displaymodulenames', 0);
        $mform->disabledIf('buttonwidth', 'displaymodulenames', 'eq', 1);
        $mform->disabledIf('iconset', 'displaymodulenames', 'eq', 1);

        // Use intro for another purpose => hide it in the form but let it exist to complete the module creation (changed later)
        $mform->addElement('html', '<div style="display:none;">');
        $mform->addElement('editor', 'introeditor', "Description", array('rows' => 10), array('maxfiles' => EDITOR_UNLIMITED_FILES,
            'noclean' => true, 'context' => $this->context, 'subdirs' => true));
        $mform->addElement('html', '</div>');
		    $mform->addElement('html', '<div style="display:none;">');
        $mform->addElement('html', '</div>');

        // State that we want to show description
        $mform->addElement('hidden', 'showdescription', 1);
        $mform->setType('showdescription', PARAM_BOOL);


        // Target section (need to access course id
        $cmid = optional_param('update', 0, PARAM_INT);
        if ($cmid) {
            $cm = $DB->get_record('course_modules', array('id' => $cmid));
            $id = $cm->course;
        }
        else {
            $id = optional_param('course', 0, PARAM_INT);
        }

        // Ensure that the course specified is valid
        if (!$course = $DB->get_record('course', array('id'=> $id))) {
            print_error('Course ID is incorrect');
        }

        $sectionname = mapmodules_extract_sections_fix($id);

        $mform->addElement('select', 'targetsection', get_string('targetsectionlabel', 'mapmodules'), $sectionname);
		$mform->setDefault('targetsection', ALL_SECTIONS_NUM);

		// Description
        $mform->addElement('header', 'descriptionpartheader', get_string('descriptionpartlabel', 'mapmodules'));
        $mform->addElement('editor', 'descriptionheadereditor', get_string('descriptionheaderlabel', 'mapmodules'),
            array('rows' => 10), array('maxfiles' => EDITOR_UNLIMITED_FILES,
            'noclean' => true, 'context' => $this->context, 'subdirs' => true));
        $mform->setType('descriptionheaderditor', PARAM_RAW); // no XSS prevention here, users must be trusted
        $mform->addElement('editor', 'descriptionfootereditor', get_string('descriptionfooterlabel', 'mapmodules'),
            array('rows' => 10), array('maxfiles' => EDITOR_UNLIMITED_FILES,
                'noclean' => true, 'context' => $this->context, 'subdirs' => true));
        $mform->setType('descriptionfootereditor', PARAM_RAW); // no XSS prevention here, users must be trusted


        //-------------------------------------------------------------------------------
        // add standard elements, common to all modules
        $this->standard_coursemodule_elements();


        //-------------------------------------------------------------------------------
        // add standard buttons, common to all modules
        $this->add_action_buttons();

        //-------------------------------------------------------------------------------
        // add information about validation (as required fields are hidden)
        $mform->addElement('static', 'mapvalidationinfo', '', get_string('mapvalidation', 'mapmodules'));
        $mform->addElement('static', 'invalidpathvalidation', '', get_string('invalidpathvalidation', 'mapmodules'));
    }

    /**
     * Prepares the form before data are set
     *
     * Additional wysiwyg editor are prepared here, the introeditor is prepared automatically by core.
     *
     * @param array $data to be set
     * @return void
     */
    public function data_preprocessing(&$data) {
        if ($this->current->instance) {
            $draftitemid = file_get_submitted_draft_itemid('descriptionheader');
            $data['descriptionheadereditor']['text'] = file_prepare_draft_area($draftitemid, $this->context->id,
                'mod_mapmodules', 'descriptionheader', 0,
                null,
                $data['descriptionheader']);
            $data['descriptionheadereditor']['format'] = $data['descriptionheaderformat'];
            $data['descriptionheadereditor']['itemid'] = $draftitemid;

            $draftitemid = file_get_submitted_draft_itemid('descriptionfooter');
            $data['descriptionfootereditor']['text'] = file_prepare_draft_area($draftitemid, $this->context->id,
                'mod_mapmodules', 'descriptionfooter', 0,
                null,
                $data['descriptionfooter']);
            $data['descriptionfootereditor']['format'] = $data['descriptionfooterformat'];
            $data['descriptionfootereditor']['itemid'] = $draftitemid;
        } else {
            $draftitemid = file_get_submitted_draft_itemid('descriptionheader');
            file_prepare_draft_area($draftitemid, null, 'mod_mapmodules', 'descriptionheader', 0);
            $data['descriptionheadereditor'] = array('text' => '', 'format' => editors_get_preferred_format(), 'itemid' => $draftitemid);

            $draftitemid = file_get_submitted_draft_itemid('descriptionfooter');
            file_prepare_draft_area($draftitemid, null, 'mod_mapmodules', 'descriptionfooter', 0);
            $data['descriptionfootereditor'] = array('text' => '', 'format' => editors_get_preferred_format(), 'itemid' => $draftitemid);
        }

    }
}
