<?php
/**
 * Generation of Map editor for Moodle form
 *
 * @package    mod_mapmodules
 * @author     Nicolas Daugas
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/course/moodleform_mod.php');
require_once(__DIR__ . '/locallib.php');
require_once($CFG->libdir . '/filelib.php');


class map_editor {

    private $contextid;

    public function __construct($contextid)
    {
        $this->contextid = $contextid;
    }

    private function get_standard_images() {
        global $CFG;

        $html = "";

        $standardmaps = json_decode(file_get_contents($CFG->dirroot."/mod/mapmodules/res/standard_maps.json"));

        $imagedirlocal = $CFG->dirroot . '/mod/mapmodules/pix/maps';
        $imagedirdistant = $CFG->wwwroot . '/mod/mapmodules/pix/maps';
        if ($handle = opendir($imagedirlocal)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    $path = '';
                    $iconset = '';
                    $buttonwidth = '';

                    // Search for standard map set
                    foreach ($standardmaps as $standardmap) {
                        if ($standardmap->mapname === $entry) {
                            $themename = $standardmap->themename;
                            $path = $standardmap->path;
                            $iconset = $standardmap->iconset;
                            $buttonwidth = $standardmap->buttonwidth;
                        }
                    }
                    $html .= "<img class='standard-map' src='$imagedirdistant/$entry' 
data-themename='$themename'
data-mapname='$entry'
height='80' width='200' alt='$entry' 
onclick='useStandardImage(\"$themename\", \"$path\", \"$iconset\", \"$buttonwidth\")'
data-dismiss='modal'>\n";
                }
            }
            closedir($handle);
        }

        return $html;
    }

    public function render() {
        global $CFG;

        $themes = $this->get_standard_images();

        $html = "
        <div id='map-editor' class='mod-mapmodules-map-container' oncontextmenu='stopEditing(event)' data-contextid='$this->contextid'>
            <img src='' class='mod-mapmodules-map-img' id='map-editor-img'>
            <div class='mod-mapmodules-icons-panel'>
                <img id='todo-icon' class='mod-mapmodules-activity-icon'/>
                <img id='checked-icon' class='mod-mapmodules-activity-icon'/>
                <img id='locked-icon' class='mod-mapmodules-activity-icon'/>
            </div>
            <svg id='map-svg' class='mod-mapmodules-map-svg' viewBox='0 0 0 0' version='1.1'>
                <path id='map-path' fill='white' d=''></path>
                <g id='map-controlpoints'></g>
            </svg>
        </div>
        
        <div class='mod-mapmodules-drawing-btn-panel'>
            <div id='action-btn-panel'>
                <button type='button' class='mod-mapmodules-drawing-btn' id='start-editing-btn' onclick='pushStartEditingBtn(event)'>
                    <span class='btn-icon'><img src='$CFG->wwwroot/mod/mapmodules/pix/mapeditorpix/pencil.svg'/></span>
                </button>
                <button type='button' class='mod-mapmodules-drawing-btn' id='reinit-btn' onclick='reinitAndStartEditing(event)'>
                    <span class='btn-icon'><img src='$CFG->wwwroot/mod/mapmodules/pix/mapeditorpix/erase.svg'/></span>
                </button>
            </div>
            <div id='style-btn-panel'>
                <button type='button' class='mod-mapmodules-drawing-btn mod-mapmodules-drawing-btn-selected' id='map-editor-style-bezier' onclick='updateStyle(event)' data-style='quickbezier'>
                    <span class='btn-icon'><img src='$CFG->wwwroot/mod/mapmodules/pix/mapeditorpix/bezier.svg'/></span>
                </button>
                <button type='button' class='mod-mapmodules-drawing-btn' id='map-editor-style-line' onclick='style=updateStyle(event)' data-style='line'>
                    <span class='btn-icon'><img src='$CFG->wwwroot/mod/mapmodules/pix/mapeditorpix/lines.svg'/></span>
                </button>
            </div>
        </div>
        
        <div class='modal fade mod-mapmodules-standard-maps-modal' id='standard-maps-modal'>
            <div class='modal-dialog mod-mapmodules-standard-maps-modal-dialog'>
                <div class='modal-content container-fluid mod-mapmodules-standard-maps-modal-container'>
        
                    <div class='modal-header mod-mapmodules-standard-maps-modal-header'>
                        <button type='button' class='close mod-mapmodules-standard-maps-modal-close' data-dismiss='modal'>x</button>
                        <div><h3>Charger une carte</h3></div>
                    </div>
                    
                    <div id='standard-maps' class='modal-body'>
                        <div>
                            <input type='checkbox' id='use-standard-path-checkbox' name='usestandardpath' checked>
                            <label for='use-standard-path-checkbox'>Charger le chemin et les icônes correspondants (ceci effacera le chemin en cours)</label>
                        </div>
                        <div>
                            $themes
                        </div>
                    </div>
                    
                </div>
            </div>    
        </div>

        <div id='loadstandardimage' class='btn btn-secondary' data-toggle='modal' data-target='#standard-maps-modal' data-backdrop='true'>" . get_string('loadstandardimage', 'mapmodules') . "</div>
        
        <script src='$CFG->wwwroot/mod/mapmodules/js/snap.svg-min.js'></script>
        <script defer src='$CFG->wwwroot/mod/mapmodules/js/createMap.js'></script>
";

        return $html;
    }
}