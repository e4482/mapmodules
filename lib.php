<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of interface functions and constants for module mapmodules
 *
 * All the core Moodle functions, neeeded to allow the module to work
 * integrated in Moodle should be placed here.
 * All the mapmodules specific functions, needed to implement all the module
 * logic, should go to locallib.php. This will help to save some memory when
 * Moodle is performing actions across all modules.
 *
 * @package    mod_mapmodules
 * @author  2015 Pascal Fautrero pascal.fautrero@ac-versailles.fr
 * @author  Nicolas Daugas
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();


define('ALL_SECTIONS_NUM', '666');

define('STANDARD_GAME_WIDTH', 1000);
define('STANDARD_GAME_HEIGHT', 400);

/**
 * Returns the information on whether the module supports a feature
 *
 * @see plugin_supports() in lib/moodlelib.php
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed true if the feature is supported, null if unknown
 */
function mapmodules_supports($feature) {
    switch($feature) {
        case FEATURE_MOD_INTRO:         return false;
        case FEATURE_SHOW_DESCRIPTION:  return true;
        case FEATURE_BACKUP_MOODLE2:    return true;
        case FEATURE_NO_VIEW_LINK	:	return true;
        default:                        return null;
    }
}

/*
 *
 *
 *
 *
 */

function get_game_source_code($mapmodules, $contextid = null){
    if (isset($mapmodules->displaymodulenames) && $mapmodules->displaymodulenames) {
        $maphtml = mapmodules_create_game_khan($mapmodules);
    } else {
        $maphtml = mapmodules_create_game($mapmodules, $contextid);
    }

    return (string) $maphtml;
}
/*
 *
 * @return array
 *
 * 		[0] => 'Toutes les sections',
 * 		[210] => 'section 1',
 * 		[215] => 'section 2',
 * 		[312] => 'vamos a la playa senor zorro'
 */

function mapmodules_extract_sections($courseid){
    global $DB;

    $sections = array();
    $sections['course'] = $courseid;
    $sectionslist = array_values($DB->get_records('course_sections', $sections));
    $sectionname =  array();
    for ($i = 0; $i < count($sectionslist);$i++) {
        if ($sectionslist[$i]->name != null) {
            $sectionname[$sectionslist[$i]->id] = $sectionslist[$i]->name;
        }
        else {
            $sectionname[$sectionslist[$i]->id] = "section " . $sectionslist[$i]->section;
        }
    }
    $sectionname[0] = "Toutes les sections";
    return $sectionname;
}

/*
 *
 * @return array
 *
 * 		[0] 	=> 'section 0',
 * 		[1] 	=> 'section 1',
 * 		[2] 	=> 'section 2',
 * 		[4] 	=> 'vamos a la playa senor zorro',
 * 		[ALL_SECTIONS_NUM] 	=> 'Toutes les sections'
 */

function mapmodules_extract_sections_fix($courseid){
    global $DB;

    $sections = array();
    $sections['course'] = $courseid;
    $sectionslist = array_values($DB->get_records('course_sections', $sections));
    $sectionname =  array();
    for ($i = 0; $i < count($sectionslist);$i++) {
        if ($sectionslist[$i]->name != null) {
            $sectionname[$sectionslist[$i]->section] = $sectionslist[$i]->name;
        }
        else {
            $sectionname[$sectionslist[$i]->section] = "section " . $sectionslist[$i]->section;
        }
    }
    $sectionname[ALL_SECTIONS_NUM] = "Toutes les sections";
    return $sectionname;
}


/*
 *
 */
function mapmodules_get_html_modules(stdClass $mapmodules) {

    global $DB, $CFG, $USER;
    include_once($CFG->dirroot.'/lib/completionlib.php');

    $sectionid = $mapmodules->targetsection;
    if ($sectionid == ALL_SECTIONS_NUM) {
        $course = $DB->get_record("course", array('id' => $mapmodules->course));
        $sections = array();
        $sections['course'] = $course->id;
        $sectionslist = array_values($DB->get_records('course_sections', $sections));
        $modulesid = Array();
        for ($i = 0; $i < count($sectionslist);$i++) {
            $sectionmodulesid = explode(",", $sectionslist[$i]->sequence);
            $modulesid = array_merge($modulesid, $sectionmodulesid);
        }
    }
    else {
        $section = $DB->get_record("course_sections", array('section' => $sectionid, 'course' => $mapmodules->course));
        $course = $DB->get_record("course", array('id' => $mapmodules->course));
        $modulesid = explode(",", $section->sequence);
    }

    $modules = array();
    $modulesnames = array();
    foreach($modulesid as $moduleid) {
        $currentmodule = $DB->get_record_sql("
			SELECT cm.*, m.name AS name
			FROM {course_modules} AS cm, {modules} AS m
			WHERE cm.id = '". $moduleid ."'
			AND m.id = cm.module
		");
        if ($currentmodule) {
            $currentinstance = $DB->get_record_sql("
				SELECT i.id AS id, i.name AS name, i.intro AS intro
				FROM {". $currentmodule->name ."} AS i
				WHERE i.id = '". $currentmodule->instance ."'
			");
            array_push($modulesnames, $currentmodule);
            array_push($modules, $currentinstance);
        }
    }

    $htmlModules = array();
    $htmlModules['list'] = "";
    $htmlModules['contents'] = "";

    $excludedModules = array("label", "mapmodules");

    $completion = new completion_info($course);
    $completion_enabled = false;
    if ($completion->is_enabled_for_site() && $completion->is_enabled()) {
        $completion_enabled = true;
        error_log("completion ok");
    }


    for($i = 0;$i < count($modules);$i++) {
        if (!in_array($modulesnames[$i]->name, $excludedModules)) {
            $modulecompletion = "";
            if ($completion_enabled && $completion->is_enabled($modulesnames[$i])) {
                $current = $completion->get_data($modulesnames[$i],null,$USER->id);
                if ($current->completionstate == COMPLETION_COMPLETE) {
                    $modulecompletion = "linkchecked";
                }
            }
            try {
            $modinfo = get_fast_modinfo($course);
            $cm = $modinfo->get_cm($modulesnames[$i]->id);
            //$cm = $currentmodule;

            if ($cm->uservisible) {
                // User can access the activity.
                $availability = "ok";
            //} else if ($cm->get_available_info()) {
            } else {
                // User cannot access the activity, but on the course page they will
                // see a link to it, greyed-out, with information (HTML format) from
                // $cm->availableinfo about why they can't access it.
                $availability = "locked";
            //} else {
            //    $availability = false;
            }
            $instancetype = $modulesnames[$i]->name;
            $instanceid = $modulesnames[$i]->id;
            if ($availability) {
                $modulename = $modules[$i]->name;
                if ($availability == "locked") {
                    if (!empty($cm->availableinfo)) {
                        $formattedinfo = \core_availability\info::format_info(
                                $cm->availableinfo, $cm->get_course());
                        $moduleintro = html_writer::tag('div', $formattedinfo, array('class' => 'availabilityinfo'));
                    }
                    $button = "";
                    $moduleintro = "<p>activité non disponible pour le moment</p>";
                }
                else {
                    $cm_mapmodules = get_coursemodule_from_instance('mapmodules', $mapmodules->id, $course->id, false, MUST_EXIST);
                    $moduleintro = $modules[$i]->intro;
                    $button = '<p style="text-align:center;"><a class="btn" href="/mod/' . $instancetype . '/view.php?id=' . $instanceid .  "&map=" . $cm_mapmodules->id . '">Démarrer l\'activité</a></p>';
                    //$context = context_module::instance($cm_mapmodules->id);
                    $modulecontext = $DB->get_record_sql("SELECT * FROM {context} WHERE instanceid = '".$cm->id."' AND contextlevel = '70'");
                    $moduleintro = file_rewrite_pluginfile_urls($moduleintro, 'pluginfile.php', $modulecontext->id, 'mod_' . $instancetype, 'intro', null);
                }



                $htmlModules['list'] .= <<<EOT
    <li><a class="link $modulecompletion $availability" href="#" data-content='content_$i'><span class="spantitle">$modulename</span></a></li>
EOT;

                $htmlModules['contents'] .= <<<EOT
    <div class="toto" id="content_$i">
        <div style="margin:20px;z-index:100;">
            <h1>$modulename</h1>
            <p>$moduleintro</p>
            $button
        </div>
    </div>
EOT;

            }
			}
			catch(Exception $e) {

			}
        }
    }

    return $htmlModules;

}
/*
 *
 *
 *
 */

function mapmodules_get_modules_from_section($data) {

  global $DB;
	$courseid = $data['courseid'];
	$sectionrank = $data['sectionrank'];
    $modulesid = Array();
    $sectionsid = Array();

    if ($sectionrank == ALL_SECTIONS_NUM) {
        $sections = array();
        $sections['course'] = $courseid;
        $sectionslist = array_values($DB->get_records('course_sections', $sections));
        for ($i = 0 ; $i < count($sectionslist) ; $i++) {
            if (!empty($sectionslist[$i]->sequence)) {
                $sectionmodulesid = explode(",", $sectionslist[$i]->sequence);
                $modulesid = array_merge($modulesid, $sectionmodulesid);
            }
        }
    }
    else {
        $section = $DB->get_record_sql("
			SELECT *
			FROM {course_sections}
			WHERE section = '" . $sectionrank . "'
			AND course = '" . $courseid . "'
        ");
        if ($section) {
			$modulesid = explode(",", $section->sequence);
		}
    }
	return $modulesid;
}


/*
 * return Array of objects
 * Array(
 *   stdClass(
 *     link : to access current module
 *     availability : "ok", "locked"
 *     desc: description of the current module
 *     completion: "linkchecked", ""
 *   )
 * )
 */
function mapmodules_get_json_modules(stdClass $mapmodules) {

    global $DB, $CFG, $USER;
    include_once($CFG->dirroot.'/lib/completionlib.php');

    $sectionrank = $mapmodules->targetsection;

    // retrieve the modules list from target section

    $modulesid = mapmodules_get_modules_from_section(array(
			'courseid' => $mapmodules->course,
			'sectionrank' => $sectionrank)
		);

    // check if completion is active
    $course = $DB->get_record("course", array('id' => $mapmodules->course));
    $completion = new completion_info($course);
    $completion_enabled = false;
    if ($completion->is_enabled_for_site() && $completion->is_enabled()) {
        $completion_enabled = true;
    }

    $excludedModules = array("label", "mapmodules");
    $modules = array();

    // for each module, build expected object

    $i = 0;
    foreach($modulesid as $moduleid) {
        $currentmodule = $DB->get_record_sql("
			SELECT cm.*, m.name AS name
			FROM {course_modules} AS cm, {modules} AS m
			WHERE cm.id = '". $moduleid ."'
      AND cm.visible = '1'
			AND m.id = cm.module
		");
        if ($currentmodule && $currentmodule->name !== 'hotpot') {
            $currentinstance = $DB->get_record_sql("
                SELECT i.name, i.intro
                FROM {". $currentmodule->name ."} AS i
                WHERE i.id = '". $currentmodule->instance ."'
            ");

            if (!in_array($currentmodule->name, $excludedModules)) {
                $newobject = new stdClass;
                $newobject->sectionid = $sectionrank;

                $cm = get_coursemodule_from_instance('mapmodules', $mapmodules->id, $course->id, false, MUST_EXIST);

                $newobject->link = $CFG->wwwroot . "/mod/" . $currentmodule->name . "/view.php?id=" . $currentmodule->id . "&map=" . $cm->id;
                //$newobject->link = $currentmodule->id;
                $modulecompletion = "";
                if ($completion_enabled && $completion->is_enabled($currentmodule)) {
                    $current = $completion->get_data($currentmodule,null,$USER->id);
                    if (($current->completionstate == COMPLETION_COMPLETE) OR ($current->completionstate == COMPLETION_COMPLETE_PASS))  {
                        $modulecompletion = "linkchecked";
                    }
                }
                $newobject->completion = $modulecompletion;
                // handle exception generated by get_cm if module does not exists
                // anymore.

                try {
                    $modinfo = get_fast_modinfo($course);
                    $cm = $modinfo->get_cm($currentmodule->id);
                    //$cm = $currentmodule;

                    if ($cm->uservisible) {

                        // User can access the activity.
                        $newobject->availability = "ok";
                        //$newobject->desc = $currentinstance->name . "\n" . strip_tags($currentinstance->intro);
                        $newobject->desc = $currentinstance->name;
                        if ($completion->is_enabled($currentmodule)) array_push($modules, $newobject);
                    //} else if ($cm->availableinfo) {
		               } else {
                        if (!empty($cm->availableinfo)) {
                            $context = \context_course::instance($modinfo->courseid);
                            $info = preg_replace_callback('~<AVAILABILITY_CMNAME_([0-9]+)/>~',
                                    function($matches) use($modinfo, $context) {
                                        $cm = $modinfo->get_cm($matches[1]);
                                        if ($cm->has_view() and $cm->uservisible) {
                                            // Help student by providing a link to the module which is preventing availability.
                                            return $cm->name;
                                        } else {
                                            return $cm->name;
                                        }
                                    }, str_replace("<strong>", "", str_replace("</strong>", "",str_replace("</li>", ".",str_replace("<li>", " ",str_replace("</ul>", "",str_replace("<ul>", " ",$cm->availableinfo)))))));

                            $newobject->desc = $info;
                        }
                        $newobject->desc = "activité non disponible pour le moment.";
                        // User cannot access the activity, but on the course page they will
                        // see a link to it, greyed-out, with information (HTML format) from
                        // $cm->availableinfo about why they can't access it.
                        $newobject->availability = "locked";
                        if ($completion->is_enabled($currentmodule)) array_push($modules, $newobject);
                      }
                    //} else {
                        // User cannot access the activity and they will not see it at all.
                    //}
                }
                catch(Exception $e) {

                }
            }
            $i++;
        }
    }
    //return json_encode($modules, JSON_PRETTY_PRINT);
    return $modules;

}

/*
 * khan theme
 */

function mapmodules_create_game_khan(stdClass $mapmodules){
    global $USER;
    $gameid = "game" . rand(100,999);
    $game = <<<EOT
<link rel="stylesheet" type="text/css" href="/mod/mapmodules/css/style.css"/>
<div id= "map" class="map"  style="display:table;">
    <div class="containermap">
        <ul id="mymapmodule" class="containermapul" style="height:100%;overflow:auto;">
            Chargement...
        </ul>

    </div>
    <div id="mymapmodulecontent" style="display:table-cell"></div>
</div>
<script>

    var hasClass = function(ele,cls) {
        return (' ' + ele.className + ' ').indexOf(' ' + cls + ' ') > -1;
    }

    var addClass = function(ele,cls) {
      if (!hasClass(ele,cls)) ele.className += " "+cls;
    }

    var removeClass = function(ele,cls) {
      if (hasClass(ele,cls)) {
        var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
        ele.className=ele.className.replace(cls,' ');
      }
    }
    var xhReq = new XMLHttpRequest();
    xhReq.open("GET", "/mod/mapmodules/ajax/validate.ajax.php?iskhan=true&mapid=$mapmodules->id&iskhan=true&sesskey=$USER->sesskey&format=html", false);
    xhReq.onreadystatechange = function (aEvt) {
        if (xhReq.readyState == 4) {
            if(xhReq.status == 200) {

                var mymapmodule = document.getElementById("mymapmodule");
                var serverResponse = JSON.parse(xhReq.responseText);
                mymapmodule.innerHTML = serverResponse.contents.list;

                var mymapmodulecontent = document.getElementById("mymapmodulecontent");
                mymapmodulecontent.innerHTML = serverResponse.contents.contents;

                var classname = document.getElementsByClassName("link");
                var listbox = document.getElementsByClassName("toto");

                var myFunction = function(ev) {
                    ev.preventDefault();
                    for(var i=0;i<listbox.length;i++){
                        removeClass(listbox[i], "linkfocus");
                    }
                    var contentid = this.getAttribute("data-content");
                    var content = document.getElementById(contentid);
                    //addClass(this.nextElementSibling, "linkfocus");
                    addClass(content, "linkfocus");
                };

                for(var i=0;i<classname.length;i++){

                    classname[i].addEventListener('click', myFunction, false);
                    if (i == 0) {
                        classname[i].click();
                    }
                }
                var map = document.getElementById("map");
                var mymapmodule = document.getElementById("mymapmodule");
                map.setAttribute("style", "min-height:200px;");
                mymapmodule.setAttribute("style", "height:"+map.offsetHeight+"px;overflow:visible;padding:0;");

            }
            else {
              console.log("Erreur pendant le chargement de la page.");
            }
        }
    };
    xhReq.send(null);

</script>
EOT;
    return $game;
}



function mapmodules_create_game(stdClass $mapmodule, $contextid){

    global $CFG, $DB;
    $gameid = "game" . rand(100,999);
    $game_width = STANDARD_GAME_WIDTH;
    $game_height = STANDARD_GAME_HEIGHT;

    $map_src = get_map_src($mapmodule, $contextid);
	$map_path = $mapmodule->path;

	$iconsets = json_decode(file_get_contents($CFG->dirroot."/mod/mapmodules/res/iconsets.json"));
	$iconset = $iconsets[$mapmodule->iconset];
	$map_button_check = get_icon_src($iconset->checked);
	$map_button_question = get_icon_src($iconset->todo);
	$map_button_lock = get_icon_src($iconset->locked);

    $button_width = $mapmodule->buttonwidth;

	ob_start();
	require($CFG->dirroot . '/mod/mapmodules/template/default.html');
	$game = ob_get_contents();
	ob_end_clean();

	if(!$contextid) {
        $contextid = context_module::instance($mapmodule->coursemodule, MUST_EXIST)->id;
    }
    $descriptionheader = str_replace('@@PLUGINFILE@@', $CFG->wwwroot . '/pluginfile.php/' . $contextid . '/mod_mapmodules/descriptionheader/0', $mapmodule->descriptionheader);
    $descriptionfooter = str_replace('@@PLUGINFILE@@', $CFG->wwwroot . '/pluginfile.php/' . $contextid . '/mod_mapmodules/descriptionfooter/0', $mapmodule->descriptionfooter);

	return "<div>$descriptionheader</div><div>$game</div><div>$descriptionfooter</div>";

}

function get_icon_src($iconname) {
    global $CFG;
    return $CFG->wwwroot ."/mod/mapmodules/pix/icons/" . $iconname;
}

function get_map_src($mapmodule, $contextid) {
    global $CFG;

    // Check if image is in standard image or in file
    $mapdata = explode(' : ', $mapmodule->name);
    $origin = $mapdata[0];

    switch ($origin) {
        case 'Carte standard':
            $maps = json_decode(file_get_contents($CFG->dirroot."/mod/mapmodules/res/standard_maps.json"));
            $mapname = null;
            foreach ($maps as $map) {
                if ($map->themename === $mapdata[1]) {
                    $mapname = $map->mapname;
                    break;
                }
            }
            if (empty($mapname)) {
                throw new Exception("Theme {$mapdata[1]} not found");
            }
            $imagename = $CFG->wwwroot ."/mod/mapmodules/pix/maps/". $mapname;
            break;

        case 'Carte personnalisée':
            $itemid = 0;
            $filename = $mapdata[1];
            $imagename = moodle_url::make_pluginfile_url($contextid, 'mod_mapmodules', 'maps', $itemid, '/', $filename);
            break;
        default:
            throw new Exception("Unknown origin of image : origin=$origin, must be 'Carte standard' or 'Carte personnalisée'.");
    }

    return $imagename;
}


/**
 * Saves a new instance of the mapmodules into the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @param object $mapmodules An object from the form in mod_form.php
 * @param mod_mapmodules_mod_form $mform
 * @return int The id of the newly inserted mapmodules record
 */
function mapmodules_add_instance(stdClass &$mapmodules, mod_mapmodules_mod_form $mform = null) {
    global $DB;

    $mapmodules->timecreated = time();
    $mapmodules->descriptionheader = '';
    $mapmodules->descriptionheaderformat = FORMAT_HTML;
    $mapmodules->descriptionfooter = '';
    $mapmodules->descriptionfooterformat = FORMAT_HTML;
    save_files($mapmodules);

    $mapmodules->id = $DB->insert_record('mapmodules', $mapmodules, true);
    $newmapmodules = $DB->get_record('mapmodules', array('id' => $mapmodules->id));

    $contextid = context_module::instance($mapmodules->coursemodule, MUST_EXIST)->id;
    $mapmodules->intro = $newmapmodules->intro = get_game_source_code($mapmodules, $contextid);
    $mapmodules->introformat = $newmapmodules->introformat = FORMAT_HTML;

    $newmapmodules->timemodified = time();

    $DB->update_record('mapmodules', $newmapmodules);

    //mapmodules_grade_item_update($mapmodules);
    return $mapmodules->id;
}

/**
 * Updates an instance of the mapmodules in the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @param object $mapmodules An object from the form in mod_form.php
 * @param mod_mapmodules_mod_form $mform
 * @return boolean Success/Fail
 */
function mapmodules_update_instance(stdClass $mapmodules, mod_mapmodules_mod_form $mform = null) {
    global $DB;

    $mapmodules->timemodified = time();
    $mapmodules->id = $mapmodules->instance;

    // process the custom wysiwyg editors
    $contextid = context_module::instance($mapmodules->coursemodule, MUST_EXIST)->id;
    if ($draftitemid = $mapmodules->descriptionheadereditor['itemid']) {
        $mapmodules->descriptionheader = file_save_draft_area_files($draftitemid, $contextid, 'mod_mapmodules', 'descriptionheader',
            0, null, $mapmodules->descriptionheadereditor['text']);
    } else {
        warn("Itemid not found for description header");
        $mapmodules->descriptionheader = $mapmodules->descriptionheadereditor['text'];
    }
    $mapmodules->descriptionheaderformat = $mapmodules->descriptionheadereditor['format'];
    if ($draftitemid = $mapmodules->descriptionfootereditor['itemid']) {
        $mapmodules->descriptionfooter = file_save_draft_area_files($draftitemid, $contextid, 'mod_mapmodules', 'descriptionfooter',
            0, null, $mapmodules->descriptionfootereditor['text']);
    } else {
        warn("Itemid not found for description footer");
        $mapmodules->descriptionfooter = $mapmodules->descriptionfootereditor['text'];
    }
    $mapmodules->descriptionfooterformat = $mapmodules->descriptionfootereditor['format'];

    save_files($mapmodules);

    $mapmodules->intro = get_game_source_code($mapmodules, $contextid);
    $mapmodules->introformat = FORMAT_HTML;

    return $DB->update_record('mapmodules', $mapmodules);
}

function save_files(&$mapmodules) {
    $contextid = context_module::instance($mapmodules->coursemodule, MUST_EXIST)->id;
    // process the custom wysiwyg editors
    if ($draftitemid = $mapmodules->descriptionheadereditor['itemid']) {
        $mapmodules->descriptionheader = file_save_draft_area_files($draftitemid, $contextid, 'mod_mapmodules', 'descriptionheader',
            0, null, $mapmodules->descriptionheadereditor['text']);
    } else {
        $mapmodules->descriptionheader = $mapmodules->descriptionheadereditor['text'];
    }
    $mapmodules->descriptionheaderformat = $mapmodules->descriptionheadereditor['format'];
    if ($draftitemid = $mapmodules->descriptionfootereditor['itemid']) {
        $mapmodules->descriptionfooter = file_save_draft_area_files($draftitemid, $contextid, 'mod_mapmodules', 'descriptionfooter',
            0, null, $mapmodules->descriptionfootereditor['text']);
    } else {
        $mapmodules->descriptionfooter = $mapmodules->descriptionfootereditor['text'];
    }
    $mapmodules->descriptionfooterformat = $mapmodules->descriptionfootereditor['format'];

    $explodedmapname = explode(' : ', $mapmodules->name);
    // Check if map file has to be updated : if true, its name is just the image name, else it is the full path for plugin or a standard map
    if (count($explodedmapname) === 1) {
        $draftitemid = $mapmodules->mapfile;
        if ($draftitemid) {
            $itemid = 0;
            $mapmodules->name = "Carte personnalisée : $mapmodules->name";
            file_save_draft_area_files($draftitemid, $contextid, 'mod_mapmodules', 'maps', $itemid);
        } else {
            $mapmodules->name = '';
        }
    }
}

/**
 * Removes an instance of the mapmodules from the database
 *
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @param int $id Id of the module instance
 * @return boolean Success/Failure
 */
function mapmodules_delete_instance($id) {
    global $DB;

    if (! $mapmodules = $DB->get_record('mapmodules', array('id' => $id))) {
        return false;
    }

    # Delete any dependent records here #

    $DB->delete_records('mapmodules', array('id' => $mapmodules->id));

    return true;
}

/**
 * Returns a small object with summary information about what a
 * user has done with a given particular instance of this module
 * Used for user activity reports.
 * $return->time = the time they did it
 * $return->info = a short text description
 *
 * @return stdClass|null
 */
function mapmodules_user_outline($course, $user, $mod, $mapmodules) {

    $return = new stdClass();
    $return->time = 0;
    $return->info = '';
    return $return;
}

/**
 * Prints a detailed representation of what a user has done with
 * a given particular instance of this module, for user activity reports.
 *
 * @param stdClass $course the current course record
 * @param stdClass $user the record of the user we are generating report for
 * @param cm_info $mod course module info
 * @param stdClass $mapmodules the module instance record
 * @return void, is supposed to echp directly
 */
function mapmodules_user_complete($course, $user, $mod, $mapmodules) {
}

/**
 * Given a course and a time, this module should find recent activity
 * that has occurred in mapmodules activities and print it out.
 * Return true if there was output, or false is there was none.
 *
 * @return boolean
 */
function mapmodules_print_recent_activity($course, $viewfullnames, $timestart) {
    return false;  //  True if anything was printed, otherwise false
}

/**
 * Prepares the recent activity data
 *
 * This callback function is supposed to populate the passed array with
 * custom activity records. These records are then rendered into HTML via
 * {@link mapmodules_print_recent_mod_activity()}.
 *
 * @param array $activities sequentially indexed array of objects with the 'cmid' property
 * @param int $index the index in the $activities to use for the next record
 * @param int $timestart append activity since this time
 * @param int $courseid the id of the course we produce the report for
 * @param int $cmid course module id
 * @param int $userid check for a particular user's activity only, defaults to 0 (all users)
 * @param int $groupid check for a particular group's activity only, defaults to 0 (all groups)
 * @return void adds items into $activities and increases $index
 */
function mapmodules_get_recent_mod_activity(&$activities, &$index, $timestart, $courseid, $cmid, $userid=0, $groupid=0) {
}

/**
 * Prints single activity item prepared by {@see mapmodules_get_recent_mod_activity()}

 * @return void
 */
function mapmodules_print_recent_mod_activity($activity, $courseid, $detail, $modnames, $viewfullnames) {
}

/**
 * Function to be run periodically according to the moodle cron
 * This function searches for things that need to be done, such
 * as sending out mail, toggling flags etc ...
 *
 * @return boolean
 * @todo Finish documenting this function
 **/
function mapmodules_cron () {
    return true;
}

/**
 * Returns all other caps used in the module
 *
 * @example return array('moodle/site:accessallgroups');
 * @return array
 */
function mapmodules_get_extra_capabilities() {
    return array();
}

////////////////////////////////////////////////////////////////////////////////
// Gradebook API                                                              //
////////////////////////////////////////////////////////////////////////////////

/**
 * Is a given scale used by the instance of mapmodules?
 *
 * This function returns if a scale is being used by one mapmodules
 * if it has support for grading and scales. Commented code should be
 * modified if necessary. See forum, glossary or journal modules
 * as reference.
 *
 * @param int $mapmodulesid ID of an instance of this module
 * @return bool true if the scale is used by the given mapmodules instance
 */
function mapmodules_scale_used($mapmodulesid, $scaleid) {
    global $DB;

    /** @example */
    if ($scaleid and $DB->record_exists('mapmodules', array('id' => $mapmodulesid, 'grade' => -$scaleid))) {
        return true;
    } else {
        return false;
    }
}

/**
 * Checks if scale is being used by any instance of mapmodules.
 *
 * This is used to find out if scale used anywhere.
 *
 * @param $scaleid int
 * @return boolean false no grade in mapmodules
 */
function mapmodules_scale_used_anywhere($scaleid) {
    return false;
}

/**
 * Creates or updates grade item for the give mapmodules instance
 *
 * Needed by grade_update_mod_grades() in lib/gradelib.php
 *
 * @param stdClass $mapmodules instance object with extra cmidnumber and modname property
 * @param mixed optional array/object of grade(s); 'reset' means reset grades in gradebook
 * @return void
 */
function mapmodules_grade_item_update(stdClass $mapmodules, $grades=null) {
    global $CFG;
    require_once($CFG->libdir.'/gradelib.php');

    /** @example */
    $item = array();
    $item['itemname'] = clean_param($mapmodules->name, PARAM_NOTAGS);
    $item['gradetype'] = GRADE_TYPE_VALUE;
    $item['grademax']  = 10;//$mapmodules->grade;
    $item['grademin']  = 0;

    grade_update('mod/mapmodules', $mapmodules->course, 'mod', 'mapmodules', $mapmodules->id, 0, null, $item);
}

/**
 * Update mapmodules grades in the gradebook
 *
 * Needed by grade_update_mod_grades() in lib/gradelib.php
 *
 * @param stdClass $mapmodules instance object with extra cmidnumber and modname property
 * @param int $userid update grade of specific user only, 0 means all participants
 * @return void
 */
function mapmodules_update_grades(stdClass $mapmodules, $userid = 0) {
    global $CFG, $DB;
    require_once($CFG->libdir.'/gradelib.php');

    /** @example */
    $grades = array(); // populate array of grade objects indexed by userid

    grade_update('mod/mapmodules', $mapmodules->course, 'mod', 'mapmodules', $mapmodules->id, 0, $grades);
}

////////////////////////////////////////////////////////////////////////////////
// File API                                                                   //
////////////////////////////////////////////////////////////////////////////////

/**
 * Returns the lists of all browsable file areas within the given module context
 *
 * The file area 'intro' for the activity introduction field is added automatically
 * by {@link file_browser::get_file_info_context_module()}
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param stdClass $context
 * @return array of [(string)filearea] => (string)description
 */
function mapmodules_get_file_areas($course, $cm, $context) {
    return array();
}

/**
 * File browsing support for mapmodules file areas
 *
 * @package mod_mapmodules
 * @category files
 *
 * @param file_browser $browser
 * @param array $areas
 * @param stdClass $course
 * @param stdClass $cm
 * @param stdClass $context
 * @param string $filearea
 * @param int $itemid
 * @param string $filepath
 * @param string $filename
 * @return file_info instance or null if not found
 */
function mapmodules_get_file_info($browser, $areas, $course, $cm, $context, $filearea, $itemid, $filepath, $filename) {
    return null;
}

/**
 * Serves the files from the mapmodules file areas
 *
 * @package mod_mapmodules
 * @category files
 *
 * @param stdClass $course the course object
 * @param stdClass $cm the course module object
 * @param stdClass $context the mapmodules's context
 * @param string $filearea the name of the file area
 * @param array $args extra arguments (itemid, path)
 * @param bool $forcedownload whether or not force download
 * @param array $options additional options affecting the file serving
 */
function mapmodules_pluginfile($course, $cm, $context, $filearea, array $args, $forcedownload, array $options=array()) {
    global $DB, $CFG;

    if ($context->contextlevel != CONTEXT_MODULE) {
        send_file_not_found();
    }

    require_login($course, true, $cm);

//    array_shift($args); // ignore revision - designed to prevent caching problems only

    $fs = get_file_storage();
    $relativepath = implode('/', $args);
    $fullpath = "/$context->id/mod_mapmodules/$filearea/$relativepath";

    if (!$file = $fs->get_file_by_hash(sha1($fullpath)) or $file->is_directory()) {
        return false;
    }

    // finally send the file
    // for folder module, we force download file all the time
    send_stored_file($file, 0, 0, true, $options);
}


////////////////////////////////////////////////////////////////////////////////
// Navigation API                                                             //
////////////////////////////////////////////////////////////////////////////////

/**
 * Extends the global navigation tree by adding mapmodules nodes if there is a relevant content
 *
 * This can be called by an AJAX request so do not rely on $PAGE as it might not be set up properly.
 *
 * @param navigation_node $navref An object representing the navigation tree node of the mapmodules module instance
 * @param stdClass $course
 * @param stdClass $module
 * @param cm_info $cm
 */
function mapmodules_extend_navigation(navigation_node $navref, stdclass $course, stdclass $module, cm_info $cm) {
}

/**
 * Extends the settings navigation with the mapmodules settings
 *
 * This function is called when the context for the page is a mapmodules module. This is not called by AJAX
 * so it is safe to rely on the $PAGE.
 *
 * @param settings_navigation $settingsnav {@link settings_navigation}
 * @param navigation_node $mapmodulesnode {@link navigation_node}
 */
function mapmodules_extend_settings_navigation(settings_navigation $settingsnav, navigation_node $mapmodulesnode=null) {
}

/**
 * Check if mapmodule is filled according to DB expectations
 */
function mapmodules_db_upgrade($mapmodule, $standardmaps = null, $mapmodulemoduleid = null) {
    global $DB;
    if ($standardmaps === null){
        global $CFG;
        $standardmaps = json_decode(file_get_contents($CFG->dirroot."/mod/mapmodules/res/standard_maps.json"));
    }

    if (empty($mapmodulemoduleid)) {
        $mapmodulemoduleid = $DB->get_record('modules', ['name' => 'mapmodules'], 'id', MUST_EXIST)->id;
    }

    if (strpos($mapmodule->name, 'Carte standard') === false && strpos($mapmodule->name, 'Carte personnalisée') === false) {
        // -> Filling fields with theme according to map json file
        foreach ($standardmaps as $standardmap) {
            if ($standardmap->themeid === $mapmodule->theme) {
                $mapmodule->name = 'Carte standard : ' . $standardmap->themename;
                $mapmodule->iconset = $standardmap->iconset;
                $mapmodule->path = $standardmap->path;
                $mapmodule->buttonwidth = $standardmap->buttonwidth;
                $mapmodule->displaymodulenames = $standardmap->displaymodulenames === "true"?1:0;
                break;
            }
        }

        // intro fields modifications : update totally to be sure that it is up to date
        // else case only arises when backuping => intro course is updated later when module is created and contextid known
        if ($module = $DB->get_record('course_modules', ['module' => $mapmodulemoduleid, 'instance' => $mapmodule->id], 'id')) {
            $moduleid = $module->id;
            $contextid = $DB->get_record('context', ['contextlevel' => CONTEXT_MODULE, 'instanceid' => $moduleid], 'id', MUST_EXIST)->id;
            $mapmodule->intro = get_game_source_code($mapmodule, $contextid);
        }

    }
}
