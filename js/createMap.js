/**
 * Style
 */
var pathInWorkLine = {
    strokeWidth: 3,
    stroke:'#666',
    fill: 'none'
}
var validatedPath = {
    strokeWidth: 2,
    stroke:'#ff0',
    fill: 'none'
}
var mainPointStroke = {
    strokeWidth: 1,
    stroke: '#c00',
    fill: '#c00'
}
var controlPointStroke = {
    strokeWidth: 2,
    stroke: '#0ff',
    fill: '#0ff'
}
var tangentsLine = {
    strokeWidth: 3,
    stroke: '#0ff',
    fill: '#0ff'
}
var coefTangent = 0.25

var nominalWidth = 1000

/**
 * ******************************* Initialisation *****************************************
 */
var snap = Snap('#map-svg')
var path = snap.select('path').attr(validatedPath)
var pathform = document.querySelector('input[name="path"]')
var pathInWork = snap.path('').attr(pathInWorkLine)
var controlPointsGroup = snap.select('#map-controlpoints')
var style = 'quickbezier'
var positionPoints = []
var currentPath = ''
var mapeditor = document.getElementById('map-editor')
var mapimage = document.getElementById('map-editor-img')
var scaleX, scaleY, offsetX, offsetY
var imagewidth, imageheight
var drawing = false
var editingBtn = document.getElementById('start-editing-btn')
var nameForm = document.getElementById('id_name')
var bezierStep = 0
var iconPanel = document.querySelector('.mod-mapmodules-icons-panel')
var contextid = mapeditor.dataset.contextid
var imageLoaded = false

/**
 * Reading JSON file
 */
var iconsets = JSON.parse(document.getElementById('iconsets').innerHTML)
var iconsselector = document.getElementById('id_iconset')
var index = iconsselector.selectedIndex
iconsselector.attributes.todo = iconsets[index].todo
iconsselector.attributes.checked = iconsets[index].checked
iconsselector.attributes.locked = iconsets[index].locked

/**
 * ******************************* Functions *****************************************
 */

/**
 * Compute some image properties
 */
var updateImageProperties = function () {
    if (!imageLoaded) return

    // Update viewbox to match image
    imagewidth = nominalWidth;
    imageheight = mapimage.naturalHeight * nominalWidth / mapimage.naturalWidth
    document.getElementById('map-svg').setAttribute('viewBox', '0 0 ' + imagewidth + ' ' + imageheight)

    // Update style
    pathInWork.attr(pathInWorkLine)
    path.attr(validatedPath)

    // Update parameters for mouse detection
    scaleX = imagewidth / mapimage.width
    scaleY = imageheight / mapimage.height

    updateDisplaySettings()
}

/**
 * Loading image
 */
var displayBackground = function(imagesrc) {
    var displayStyle
    if (imagesrc) {
        displayStyle = 'block'
    } else {
        displayStyle = 'none'
    }
    mapimage.setAttribute('src', imagesrc)
    mapimage.style.display = displayStyle
    document.querySelector('.mod-mapmodules-drawing-btn-panel').style.display = displayStyle
    document.querySelector('.mod-mapmodules-icons-panel').style.display = displayStyle
    document.getElementById('map-svg').style = displayStyle

    imageLoaded = true

    // Wait image to be fully loaded to get real size for viewBox
    setTimeout(function () {
        updateImageProperties()
        validateMap()
    }, 100)

}

/**
 * Use the proper image : loaded from Moodle pickup file
 */
var useLoadedImage = function () {
    // find loaded image url
    var imagelink = document.querySelector('#fitem_id_mapfile a')
    if (!imagelink) {
        // alert there is no image loaded
        console.log('no file')
        return
    }


    var imageurl = imagelink.getAttribute('href')

    // Set name to retrieve image later
    var imageurlSplit = imageurl.split("/")
    // Don't encode twice UTF chars and don't take what is after "?" char.
    var imageName = decodeURIComponent(imageurlSplit[imageurlSplit.length-1])
    imageName = imageName.split("?")[0]
    nameForm.value = imageName

    // Display
    displayBackground(imageurl)

    // Display button to retrieve image after switch with standard map
    //document.getElementById('id_useloadedimage').style.display = 'block'
}

var useStandardImage = function (themeName, path, iconset, buttonwidth) {
    var mapName = document.getElementById('standard-maps').querySelector('[data-themename="' + themeName + '"]').dataset.mapname
    displayBackground(M.cfg.wwwroot + '/mod/mapmodules/pix/maps/' + mapName)
    nameForm.value = 'Carte standard : ' + themeName
    if (document.getElementById('use-standard-path-checkbox').checked) {
        if (path !== '') {
            reinitPath()
            pathToPoints(path.split(' '))
        }
        if (iconset !== '') {
            document.getElementById('id_iconset').value = iconset
        }
        if (buttonwidth !== '') {
            document.getElementById('id_buttonwidth').value = buttonwidth
        }
        updateDisplaySettings()
    }
}

/**
 * Display parameters buttons
 */
var updateIcon = function(selectedElement, previewElement, size) {
    previewElement.src = M.cfg.wwwroot + '/mod/mapmodules/pix/icons/' + selectedElement
    previewElement.style.width = size + "px"
    previewElement.style.height = size + "px"
}

var updateDisplaySettings = function() {
    var size = document.getElementById('id_buttonwidth').value * mapimage.width / mapimage.naturalWidth
    var index = document.getElementById('id_iconset').selectedIndex
    iconsselector.attributes.todo = iconsets[index].todo
    iconsselector.attributes.checked = iconsets[index].checked
    iconsselector.attributes.locked = iconsets[index].locked
    updateIcon(
        iconsselector.attributes.todo,
        document.getElementById('todo-icon'),
        size
    )
    updateIcon(
        iconsselector.attributes.checked,
        document.getElementById('checked-icon'),
        size
    )
    updateIcon(
        iconsselector.attributes.locked,
        document.getElementById('locked-icon'),
        size
    )
    iconPanel.style.top = (mapimage.height - size - 5) + "px"
}

/*
 * Display utilitary functions
 */
var moveStart = function () {
    // Store initial data
    this.xOrig = parseInt(this.attr('cx'))
    this.yOrig = parseInt(this.attr('cy'))
    this.dx = 0;
    this.dy = 0;
    this.xT1Orig = null
    this.xT2Orig = null
}

var drawMainPoint = function (x,y, index) {
    var point = controlPointsGroup.select('circle[indexPoint="' + index + '"]')
    if (!point) {
        controlPointsGroup.circle(x, y, 6).attr(mainPointStroke).attr({indexPoint: index}).data({
                type: 'main',
                index: index
        }).addClass('mod-mapmodules-map-point').drag(moveMainPoint, moveStart, updatePath)
    } else {
        point.attr({cx: x, cy: y}).transform('')
    }
}

var drawControlPoint = function (xCP,yCP, xMP, yMP, index, type) {
    var groupCP = controlPointsGroup.select('g[indexPoint~="' + index + type + '"]')
    // Update value if already created
    if (groupCP) {
        groupCP.transform('')
        controlPointsGroup.select('circle[indexPoint="' + index + type + '"]').attr({cx: xCP, cy: yCP}).transform('')
        controlPointsGroup.select('line[indexPoint="' + index + type + '"]').attr({x1: xMP, y1: yMP, x2: xCP, y2: yCP})
    } else {
        // Draw control point itself
        var point = controlPointsGroup.circle(xCP, yCP, 5).attr(controlPointStroke).attr({indexPoint: index.toString() + type})
        point.data({
            type: type,
            index: index
        }).addClass('mod-mapmodules-map-point').drag(moveControlPoint, moveStart, updatePath).transform('')

        // Draw line
        var line = controlPointsGroup.line(xMP, yMP, xCP, yCP).attr(tangentsLine).attr({indexPoint: index.toString() + type})
        controlPointsGroup.group(point, line).attr({indexPoint: index.toString() + type + ' ' + index})
    }
}

/**
 * Convert single position point to string in SVG path format
 * @param positionPoint
 * @returns part of "d" property of path
 */
function writePath(positionPoint) {
    switch (positionPoint.type) {
        case 'moveto':
            return 'M ' + positionPoint.x + ' ' + positionPoint.y
        case 'lineto':
            return ' L ' + positionPoint.x + ' ' + positionPoint.y
        case 'cubic':
            return ' C ' + positionPoint.firstControlPoint.x + ' ' + positionPoint.firstControlPoint.y +
                ' ' + positionPoint.lastControlPoint.x + ' ' + positionPoint.lastControlPoint.y +
                ' ' + positionPoint.x + ' ' + positionPoint.y
        case 'smooth':
            return ' S ' + positionPoint.lastControlPoint.x + ' ' + positionPoint.lastControlPoint.y +
                ' ' + positionPoint.x + ' ' + positionPoint.y
    }
}

/**
 * Update the path drawed and in form from the positionPoints
 */
function updatePath() {
    if (drawing) return

    currentPath = ''
    var positionPoint, xCP, yCP, previousPositionPoint
    for (var i=0; i<positionPoints.length ; i++) {
        positionPoint = positionPoints[i]

        // Redraw
        drawMainPoint(positionPoint.x, positionPoint.y, i)
        switch (positionPoint.type) {
            case 'cubic':
                previousPositionPoint = positionPoints[i-1]
                drawControlPoint(positionPoint.firstControlPoint.x, positionPoint.firstControlPoint.y,
                    previousPositionPoint.x, previousPositionPoint.y,
                    i, 'firstTangent')
                drawControlPoint(positionPoint.lastControlPoint.x, positionPoint.lastControlPoint.y,
                    positionPoint.x, positionPoint.y,
                    i, 'secondTangent')
                break
            case 'smooth':
                previousPositionPoint = positionPoints[i-1]
                xCP = 2 * previousPositionPoint.x - previousPositionPoint.lastControlPoint.x
                yCP = 2 * previousPositionPoint.y - previousPositionPoint.lastControlPoint.y
                // Robustness: if smooth position point is not removed yet
                if (typeof(positionPoint.lastControlPoint) === "undefined") {
                    console.error("Le point " + i + " n'est pas complètement définie.")
                    if (i === positionPoints.length-1) {
                        positionPoints.pop()
                    }
                } else {
                    drawControlPoint(xCP, yCP,
                        previousPositionPoint.x, previousPositionPoint.y,
                        i, 'firstTangent')
                    drawControlPoint(positionPoint.lastControlPoint.x, positionPoint.lastControlPoint.y,
                        positionPoint.x, positionPoint.y,
                        i, 'secondTangent')
                }
                break
        }

        // Rewrite path
        currentPath += writePath(positionPoint)
    }

    path.attr({d: currentPath})
    pathform.value = currentPath

    validateMap()
}

var simpleMove = function (object, dx, dy) {
    object.dx = Math.round(scaleX * dx)
    object.dy = Math.round(scaleY * dy)

    object.transform(
        't' + object.dx + ',' + object.dy
    )
}

var moveMainPoint = function(dx, dy) {
    if (drawing) return

    var index = this.data('index')

    // Move main point
    simpleMove(this, dx, dy)
    var currentPositionPoint = positionPoints[index];
    currentPositionPoint.x = this.xOrig + this.dx
    currentPositionPoint.y = this.yOrig + this.dy

    // Move tangents control points
    var groupCP = controlPointsGroup.select('g[indexPoint~="' + index + 'secondTangent"]')
    if (groupCP) {
        simpleMove(groupCP, dx, dy)
        if (!this.xT2Orig) {
            this.xT2Orig = currentPositionPoint.lastControlPoint.x
            this.yT2Orig = currentPositionPoint.lastControlPoint.y
        }
        currentPositionPoint.lastControlPoint.x = this.xT2Orig + this.dx
        currentPositionPoint.lastControlPoint.y = this.yT2Orig + this.dy
    }
    groupCP = controlPointsGroup.select('g[indexPoint~="' + (index+1) + 'firstTangent"]')
    if (groupCP) {
        simpleMove(groupCP, dx, dy)
        var nextPositionPoint = positionPoints[index+1]
        if (!this.xT1Orig) {
            this.xT1Orig = nextPositionPoint.firstControlPoint.x
            this.yT1Orig = nextPositionPoint.firstControlPoint.y
        }
        nextPositionPoint.firstControlPoint.x = this.xT1Orig + this.dx
        nextPositionPoint.firstControlPoint.y = this.yT1Orig + this.dy
    }

    updatePath()
}

var moveControlPoint = function (dx, dy) {
    if (drawing) return

    var index = this.data('index')
    var type = this.data('type')


    // Move point
    simpleMove(this, dx, dy)

    // Recalculate positions of parameters and drawing elements
    controlPointsGroup.select('line[indexPoint="' + index + type + '"]').attr({
        x2: this.xOrig + this.dx,
        y2: this.yOrig + this.dy
    })
    var controlPoint, positionPoint
    switch (type) {
        case 'secondTangent':
            controlPoint = positionPoints[index].lastControlPoint
            controlPoint.x = this.xOrig + this.dx
            controlPoint.y = this.yOrig + this.dy

            // Check if next is smooth
            if (index < positionPoints.length - 1) {
                if (positionPoints[index+1].type == 'smooth') {
                    // Redraw symetrically other tangent
                    var x = 2 * positionPoints[index].x - (this.xOrig + this.dx)
                    var y = 2 * positionPoints[index].y - (this.yOrig + this.dy)
                    controlPointsGroup.select('line[indexPoint="' + (index+1) + 'firstTangent"]').attr({
                        x2: x,
                        y2: y,
                    })
                    controlPointsGroup.select('circle[indexPoint="' + (index+1) + 'firstTangent"]').attr({
                        cx: x,
                        cy: y,
                    })
                }
            }

            break
        case 'firstTangent':
            positionPoint = positionPoints[index-1]
            if (positionPoints[index].type === 'smooth') {
                // Control last control point
                controlPoint = positionPoint.lastControlPoint
                controlPoint.x = 2 * positionPoint.x - (this.xOrig + this.dx)
                controlPoint.y = 2 * positionPoint.y - (this.yOrig + this.dy)

                // Redraw
                controlPointsGroup.select('line[indexPoint="' + (index-1) + 'secondTangent"]').attr({
                    x2: controlPoint.x,
                    y2: controlPoint.y,
                })
                controlPointsGroup.select('circle[indexPoint="' + (index-1) + 'secondTangent"]').attr({
                    cx: controlPoint.x,
                    cy: controlPoint.y,
                })
            } else {
                controlPoint = positionPoints[index].firstControlPoint
                controlPoint.x = this.xOrig + this.dx
                controlPoint.y = this.yOrig + this.dy
            }
            break
        default:
            console.error('Unknown type of controle point: ' + type)
    }


    updatePath()
}

/*
 *
 * Buttons effects
 *
 */

var startEditing = function (event) {
    event.preventDefault()
    event.stopPropagation()

    //prepare edition
    mapeditor.classList.add('mod-mapmodules-map-container-editing')
    editingBtn.classList.add('mod-mapmodules-drawing-btn-selected')
    editingBtn.focus()
    drawing = true
    bezierStep = 0
    computeOffsets()
}

var pushStartEditingBtn = function (event) {
    event.preventDefault()
    event.stopPropagation()

    if (drawing) {
        stopEditing(event)
    } else {
        startEditing(event)
    }
}

var reinitPath = function () {

    path.attr(validatedPath)

    // Reinit position points
    positionPoints = []
    // Update path for form and drawing
    updatePath()
    // Clear control points
    document.getElementById('map-controlpoints').innerHTML = ''
}

var reinitAndStartEditing = function (event) {
    event.stopPropagation()
    event.preventDefault()

    stopEditing(event)

    reinitPath()

    startEditing(event)
}

var stopEditing = function (event) {
    if (!drawing) {
        return
    }

    event.preventDefault()

    // Stop edition
    drawing = false
    bezierStep = 0
    mapeditor.classList.remove('mod-mapmodules-map-container-editing')
    editingBtn.classList.remove('mod-mapmodules-drawing-btn-selected')

    // Erase temp graphics elements
    pathInWork.attr({d: ''})
    var group;
    while(group = controlPointsGroup.select('g[indexPoint~="' + positionPoints.length + '"]')){
        group.remove()
    }
    
    // Save points and path
    updatePath()
}

var updateStyle = function (event) {
    event.stopPropagation()

    document.querySelector('#style-btn-panel .mod-mapmodules-drawing-btn-selected').classList.remove('mod-mapmodules-drawing-btn-selected')
    var btn = event.target
    while (!btn.classList.contains('mod-mapmodules-drawing-btn')) {
        btn = btn.parentElement
    }

    style = btn.dataset.style
    btn.classList.add('mod-mapmodules-drawing-btn-selected')

    stopEditing(event)
    startEditing(event)
}

/*
 *
 * Path computation
 *
 */
function computeOffsets() {
    var parent = mapimage
    offsetX = 0
    offsetY = 0
    while(parent) {
        offsetX += parent.offsetLeft
        offsetY += parent.offsetTop
        parent = parent.offsetParent
    }
}
function getAccurateX(event) {
    var relativex = event.pageX - offsetX
    return Math.round(scaleX * relativex)
}
function getAccurateY(event) {
    var relativey = event.pageY - offsetY
    return Math.round(scaleY * relativey)
}

/**
 * When mouse is moved, a path is computed and stored in position points array
 * @param event
 */
var updateMousePosition = function(event) {
    // If not drawing, edit elements by using specific functions
    if (!drawing) {
        return
    }

    // Mouse detection
    var x = getAccurateX(event)
    var y = getAccurateY(event)

    //console.log('Point visé : (' + x + ', ' + y + ')')

    switch (style) {
        case 'line':
            if (positionPoints.length > 0) {
                var lastpoint = positionPoints[positionPoints.length - 1]
                pathInWork.attr({
                    d: 'M ' + lastpoint.x + ',' + lastpoint.y
                        + ' L ' + x + ',' + y
                })
            }
            break
        case 'quickbezier':
            // if there is no point, nothing to do, waiting for first point to be set
            if (positionPoints.length > 0) {

                // Initial case
                if (positionPoints[positionPoints.length-1].type === 'moveto') {
                    // Draw line between first point and current whatever type of curve it is
                    var lastPoint = positionPoints[positionPoints.length-1]
                    pathInWork.attr({d: 'M ' + lastPoint.x + ',' + lastPoint.y
                            + ' L ' + x + ',' + y})
                    break
                } else if (bezierStep > 0) {    // Going on a smooth Bezier path
                    // Using 3 points : A, B, C (mouse on C)
                    var xA = positionPoints[positionPoints.length-2].x
                    var yA = positionPoints[positionPoints.length-2].y
                    var xB = positionPoints[positionPoints.length-1].x
                    var yB = positionPoints[positionPoints.length-1].y
                    // note: xC = x, yC = y

                    var movetoCommand = 'M ' + xA + ',' + yA

                    // Compute tangent for point n-1 : v = kCA
                    var xT = coefTangent * (xA - x)
                    var yT = coefTangent * (yA - y)

                    // Computing last tangent as a symetrical image of -v : v' = -v + 2 (v.BC) / BC^2
                    var kBC = xT*(x - xB) + yT*(y - yB) // first scalar product
                    var dBC2 = Math.pow(x - xB, 2) + Math.pow(y-yB, 2)
                    var xTf = -xT + 2 * kBC * (x - xB) / dBC2
                    var yTf = -yT + 2 * kBC * (y - yB) / dBC2

                    // Getting first tangent if exist, if it doesn't, compute it as a symetrical
                    var xTi, yTi
                    switch (positionPoints[positionPoints.length-2].type) {
                        case 'moveto':
                            // Compute first tangent as a symetrical image of v : v' = v - 2 (v.AB)AB / AB^2
                            var kAB = xT * (xB - xA) + yT * (yB - yA) // first scalar product
                            var dAB2 = Math.pow(xB - xA, 2) + Math.pow(yB-yA, 2)
                            xTi = xT - 2 * kAB * (xB - xA) / dAB2
                            yTi = yT - 2 * kAB * (yB - yA) / dAB2
                            break
                        case 'lineto':
                            var pointNm2 = positionPoints[positionPoints.length - 2]
                            var pointNm3 = positionPoints[positionPoints.length - 3] // exist as line to is never first, moveto is
                            // Smooth tangent
                            xTi = coefTangent * (pointNm2.x - pointNm3.x)
                            yTi = coefTangent * (pointNm2.y - pointNm3.y)
                            break
                        default:
                            var pointNm2 = positionPoints[positionPoints.length - 2]
                            xTi = -(pointNm2.lastControlPoint.x - pointNm2.x)
                            yTi = -(pointNm2.lastControlPoint.y - pointNm2.y)
                            break
                    }

                    // Store data
                    positionPoints[positionPoints.length-1].lastControlPoint = {
                        x: xB + xT,
                        y: yB + yT
                    }
                    positionPoints[positionPoints.length-1].firstControlPoint = {
                        x: xA + xTi,
                        y: yA + yTi
                    }


                    // Draw control points and tangents
                    drawControlPoint(xA+xTi, yA+yTi, xA, yA, positionPoints.length-1, 'firstTangent')
                    drawControlPoint(xB+xT, yB+yT, xB, yB, positionPoints.length-1, 'secondTangent')
                    drawControlPoint(xB-xT, yB-yT, xB, yB, positionPoints.length, 'firstTangent')
                    drawControlPoint(x+xTf, y+yTf, x, y, positionPoints.length, 'secondTangent')

                    // Draw work path
                    pathInWork.attr({d: movetoCommand +
                            ' C ' + (xA + xTi) + ' ' + (yA + yTi) + ' ' + (xB+xT) + ' ' + (yB+yT) + ' ' + xB + ' ' + yB +
                            ' S ' + (x + xTf) + ' ' + (y + yTf) + ' ' + x + ' ' + y
                    })

                } else {    // Resume a Bezier path
                    // Using 2 points : B, C (mouse on C) and their tangents
                    var pointB = positionPoints[positionPoints.length-1]
                    var xB = pointB.x
                    var yB = pointB.y
                    // note: xC = x, yC = y

                    var xTi, yTi, xTf, yTf
                    switch (positionPoints[positionPoints.length-1].type) {
                        case 'lineto':
                            var xA = positionPoints[positionPoints.length-2].x
                            var yA = positionPoints[positionPoints.length-2].y
                            // Smooth tangent
                            xTi = coefTangent * (xB - xA)
                            yTi = coefTangent * (yB - yA)
                            break
                        case 'cubic':
                        case 'smooth':
                            xTi = -(pointB.lastControlPoint.x - xB)
                            yTi = -(pointB.lastControlPoint.y - yB)
                            break
                        default:
                            console.error("Not manage case : " + positionPoints[positionPoints.length-1].type)
                    }

                    // Computing last tangent as a symetrical image of -v : v' = v - 2 (v.BC) / BC^2
                    var kBC = xTi*(x - xB) + yTi*(y - yB) // first scalar product
                    var dBC2 = Math.pow(x - xB, 2) + Math.pow(y-yB, 2)
                    var xTf = xTi - 2 * kBC * (x - xB) / dBC2
                    var yTf = yTi - 2 * kBC * (y - yB) / dBC2


                    // Draw control points and tangents
                    drawControlPoint(xB+xTi, yB+yTi, xB, yB, positionPoints.length, 'firstTangent')
                    drawControlPoint(x+xTf, y+yTf, x, y, positionPoints.length, 'secondTangent')

                    // Draw work path
                    pathInWork.attr({d: 'M ' + xB + ' ' + yB +
                            ' C ' + (xB + xTi) + ' ' + (yB + yTi) + ' ' + (x + xTf) + ' ' + (y + yTf) + ' ' + x + ' ' + y
                    })


                }
            }

    }
}

/**
 * When mouse is clicked, path is updated with computed points and is ready to welcome a new point
 * @param event
 */
var validatePoint = function(event) {
    // Mouse detection
    var x = getAccurateX(event)
    var y = getAccurateY(event)

    // Check if point is not at the same position (double clic managed by event, this case is a user manipulation error)
    if (positionPoints.length > 0) {
        let oldX = positionPoints[positionPoints.length-1].x
        let oldY = positionPoints[positionPoints.length-1].y
        if (oldX === x && oldY === y) {
            // Delete last position point
            controlPointsGroup.select('circle[indexPoint="' + (positionPoints.length-1) + '"]').remove()
            positionPoints.pop()
            stopEditing(event)
            return
        }
    }

    //console.log('Point ' + (positionPoints.length + 1) + ' visé : (' + x + ', ' + y + ')')

    drawMainPoint(x,y, positionPoints.length)

    if (positionPoints.length === 0) {
        // Set first point
        currentPath = 'M ' + x + ' ' + y
        positionPoints.push({
            x: x,
            y: y,
            type: 'moveto'
        })
    } else {
        switch (style) {
            case 'line':
                currentPath += ' L ' + x + ' ' + y
                positionPoints.push({
                    x: x,
                    y: y,
                    type: 'lineto'
                })
                break
            case 'quickbezier':
                var lastType = positionPoints[positionPoints.length-1].type
                if (bezierStep === 0) {
                    // Do not update saved path but store point for new computation, parameters updated later
                    positionPoints.push({
                        x: x,
                        y: y,
                        type: 'cubic'
                    })
                } else {
                    positionPoints.push({
                        x: x,
                        y: y,
                        type: 'smooth'
                    })
                    // Save previous point to validate path. Last one will be save on exit
                    currentPath += writePath(positionPoints[positionPoints.length-2])
                }
                bezierStep++
        }
    }

    // Update path but not entirely as some parameters of last point may change
    path.attr('d', currentPath)
}

// Rewrite points from currentPath
var pathToPoints = function (explodedPath) {
    positionPoints = []

    var offsetX, offsetY, lastTPX, lastTPY
    var lastValidCommand = 'M'
    for (var i=0 ; i<explodedPath.length; i++) {
        switch (explodedPath[i]) {
            case 'M':
            case 'm':
                // Validate
                if (i + 2 <= explodedPath.length) {
                    positionPoints.push({
                        type: 'moveto',
                        x: parseInt(explodedPath[i+1]),
                        y: parseInt(explodedPath[i+2])
                    })
                } else {
                    console.error('Les données chargées sont corrompues')
                }
                i += 2
                lastValidCommand = 'l'
                break
            case 'l':
                offsetX = positionPoints[positionPoints.length-1].x
                offsetY = positionPoints[positionPoints.length-1].y

                // Validate
                if (i + 2 <= explodedPath.length) {
                    positionPoints.push({
                        type: 'lineto',
                        x: parseInt(explodedPath[i+1]) + offsetX,
                        y: parseInt(explodedPath[i+2]) + offsetY
                    })
                } else {
                    console.error('Les données chargées sont corrompues')
                }
                lastValidCommand = 'l'
                i += 2
                break
            case 'L':
                // Validate
                if (i + 2 <= explodedPath.length) {
                    positionPoints.push({
                        type: 'lineto',
                        x: parseInt(explodedPath[i+1]),
                        y: parseInt(explodedPath[i+2])
                    })
                } else {
                    console.error('Les données chargées sont corrompues')
                }
                lastValidCommand = 'L'
                i += 2
                break
            case 'c':
                offsetX = positionPoints[positionPoints.length-1].x
                offsetY = positionPoints[positionPoints.length-1].y

                // Validate
                if (i + 6 <= explodedPath.length) {
                    positionPoints.push({
                        type: 'cubic',
                        x: parseInt(explodedPath[i+5]) + offsetX,
                        y: parseInt(explodedPath[i+6]) + offsetY,
                        firstControlPoint: {
                            x: parseInt(explodedPath[i+1]) + offsetX,
                            y: parseInt(explodedPath[i+2]) + offsetY
                        },
                        lastControlPoint: {
                            x: parseInt(explodedPath[i+3]) + offsetX,
                            y: parseInt(explodedPath[i+4]) + offsetY
                        }
                    })
                } else {
                    console.error('Les données chargées sont corrompues')
                }
                lastValidCommand = 'c'
                i += 6
                break
            case 'C':
                // Validate
                if (i + 6 <= explodedPath.length) {
                    positionPoints.push({
                        type: 'cubic',
                        x: parseInt(explodedPath[i+5]),
                        y: parseInt(explodedPath[i+6]),
                        firstControlPoint: {
                            x: parseInt(explodedPath[i+1]),
                            y: parseInt(explodedPath[i+2])
                        },
                        lastControlPoint: {
                            x: parseInt(explodedPath[i+3]),
                            y: parseInt(explodedPath[i+4])
                        }
                    })
                } else {
                    console.error('Les données chargées sont corrompues')
                }
                lastValidCommand = 'C'
                i += 6
                break
            case 's':
                offsetX = positionPoints[positionPoints.length-1].x
                offsetY = positionPoints[positionPoints.length-1].y
                lastTPX = positionPoints[positionPoints.length-1].firstControlPoint.x
                lastTPY = positionPoints[positionPoints.length-1].firstControlPoint.y

                // Validate
                if (i + 4 <= explodedPath.length) {
                    positionPoints.push({
                        type: 'smooth',
                        x: parseInt(explodedPath[i+3]) + offsetX,
                        y: parseInt(explodedPath[i+4]) + offsetY,
                        firstControlPoint: {
                            x: parseInt(explodedPath[i+3]) + offsetX - lastTPX,
                            y: parseInt(explodedPath[i+4]) + offsetY - lastTPY
                        },
                        lastControlPoint: {
                            x: parseInt(explodedPath[i+1]) + offsetX,
                            y: parseInt(explodedPath[i+2]) + offsetY
                        }
                    })
                } else {
                    console.error('Les données chargées sont corrompues')
                }
                lastValidCommand = 's'
                i += 4
                break
            case 'S':
                lastTPX = positionPoints[positionPoints.length-1].firstControlPoint.x
                lastTPY = positionPoints[positionPoints.length-1].firstControlPoint.y

                // Validate
                if (i + 4 <= explodedPath.length) {
                    positionPoints.push({
                        type: 'smooth',
                        x: parseInt(explodedPath[i+3]),
                        y: parseInt(explodedPath[i+4]),
                        firstControlPoint: {
                            x: parseInt(explodedPath[i+3]) - lastTPX,
                            y: parseInt(explodedPath[i+4]) - lastTPY
                        },
                        lastControlPoint: {
                            x: parseInt(explodedPath[i+1]),
                            y: parseInt(explodedPath[i+2])
                        }
                    })
                } else {
                    console.error('Les données chargées sont corrompues')
                }
                lastValidCommand = 'S'
                i += 4
                break
            default:
                explodedPath[i-1] = lastValidCommand
                i -= 2;
        }
    }

    updatePath()
}

var clickMap = function (event) {
    // Prevent stoppind edition when clic in page
    event.stopPropagation()

    if (drawing) {
        validatePoint(event);
    }
}

var clickOutsideMap = function (event) {
    if (drawing) {
        stopEditing(event)
    }
}

var oldImage = ''
var oldPath = ''
var oldButtonWidth = 50
var oldName = ''
var toKhanSystem = function () {
    oldImage = mapimage.getAttribute('src')
    oldPath = currentPath
    oldButtonWidth = document.getElementById('id_buttonwidth').value
    oldName = nameForm.value
    reinitPath()
    useStandardImage('khan', 'M -100 -100 1 0', '', 0)
    document.querySelector('.mod-mapmodules-drawing-btn-panel').style.display = 'none'
    updateDisplaySettings()
}
var toNormalSystem = function () {
    nameForm.value = oldName
    displayBackground(oldImage)

    reinitPath()
    pathToPoints(oldPath.split(' '))
    document.getElementById('id_buttonwidth').value = oldButtonWidth
}

/**
 * ******************************* Validation *****************************************
 */
var noMapInfo = document.getElementById('fitem_id_mapvalidationinfo')
var invalidPathInfo = document.getElementById('fitem_id_invalidpathvalidation')
var submitbutton1 = document.querySelector('input[name="submitbutton"]')
var submitbutton2 = document.querySelector('input[name="submitbutton2"]')

var validateMap = function() {
    var isValid = nameForm.value

    // Check map
    if (nameForm.value) {
        noMapInfo.style.display = 'none'
    } else {
        noMapInfo.style.display = 'block'
    }

    if (positionPoints.length >= 2) {
        invalidPathInfo.style.display = 'none'
    } else {
        invalidPathInfo.style.display = 'block'
        isValid = false
    }

    if (isValid) {
        submitbutton1.classList.remove('mod-mapmodules-invalidformbtn')
        submitbutton2.classList.remove('mod-mapmodules-invalidformbtn')
        submitbutton1.type = 'submit'
        submitbutton2.type = 'submit'
    } else {
        submitbutton1.classList.add('mod-mapmodules-invalidformbtn')
        submitbutton2.classList.add('mod-mapmodules-invalidformbtn')
        submitbutton1.type = 'button'
        submitbutton2.type = 'button'
    }

    return isValid
}

var watch = function () {
    updateImageProperties()
    setTimeout(watch, 1000)
}

/**
 * ******************************* Main instructions *****************************************
 */


/**
 * If an image is already in module (edition case)
 */
if (nameForm.value != null && nameForm.value != '') {
    var splitName = nameForm.value.split(' : ')
    if (splitName[0] === 'Carte personnalisée') {
        displayBackground(M.cfg.wwwroot + '/pluginfile.php/' + contextid +
            '/mod_mapmodules/maps/0/' + splitName[1])
    } else {
        useStandardImage(splitName[1], '', '', '')
    }

}



// Load path if already created (edition case)
if (pathform.value) {
    pathToPoints(pathform.value.split(' '))
    updatePath()
}

// Check if module is already validated
validateMap()

// Loading image
document.getElementById('id_mapfile').onchange = useLoadedImage

// Edition
mapeditor.onmousemove = updateMousePosition
mapeditor.onclick = clickMap
mapeditor.ondblclick = stopEditing
document.querySelector('body').addEventListener('click', clickOutsideMap)

// Icons selection
document.getElementById('id_buttonwidth').onchange = updateDisplaySettings
document.getElementById('id_iconset').onchange = updateDisplaySettings

// UI changes
document.querySelector('#fitem_id_mapfile input').parentNode.appendChild(document.getElementById('loadstandardimage'))

// Khan management
document.getElementById('id_displaymodulenames').onchange = function (event) {
    if (document.getElementById('id_displaymodulenames').checked) {
        toKhanSystem()
    } else {
        toNormalSystem()
    }
}

// Resize event
window.addEventListener('resize', updateImageProperties)
window.addEventListener('resize', computeOffsets)

// Launch watcher for map size
watch()