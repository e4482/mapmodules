var maploader = function(params) {

  var mapid = params.mapid
  var gameid = params.gameid
  var gamewidth = params.gamewidth
  var mapbuttoncheck = params.mapbuttoncheck
  var mapbuttonquestion = params.mapbuttonquestion
  var mapbuttonlock = params.mapbuttonlock
  var buttonwidth = params.buttonwidth

  var xhReq = new XMLHttpRequest();
  xhReq.open("GET", M.cfg.wwwroot + "/mod/mapmodules/ajax/validate.ajax.php?mapid=" + mapid + "&iskhan=false&format=json", false);
  xhReq.onreadystatechange = function (aEvt) {
      if (xhReq.readyState == 4) {
          if(xhReq.status == 200) {
              var serverResponse = JSON.parse(xhReq.responseText);
              var activities = serverResponse;
              var nbActivities = activities.length;
              var img = document.getElementById("back_" + gameid);
              var container = document.getElementById("container_" + gameid);
              var path_container = document.getElementById("path_container_" + gameid);
              var path = document.getElementById("path");
              var imagewidth = img.naturalWidth
              var imageheight = img.naturalHeight

              path_container.setAttribute("width", img.getBoundingClientRect().width);
              path_container.setAttribute("height", img.getBoundingClientRect().height);
              var viewBoxHeight = imageheight * 1000 / imagewidth
              path_container.setAttribute('viewBox', '0 0 1000 ' + viewBoxHeight)

              var imageScale = img.width / imagewidth;
              var imageSvgScale = img.width / 1000;
              var step = path.getTotalLength();

              if (nbActivities > 1) {
                step = path.getTotalLength() / (nbActivities - 1);
              }

              for (var i = 0; i < nbActivities;i++) {
                  var link = document.createElement("a");
                  link.setAttribute("id","link_" + gameid + i);
                  var ball = document.createElement("img");
                  if (activities[i].availability == "ok") {
                      if (activities[i].completion == "linkchecked") {
                          ball.setAttribute("src", mapbuttoncheck);
                      }
                      else {
                          ball.setAttribute("src", mapbuttonquestion);
                      }
                      link.setAttribute("href",activities[i].link);
                  }
                  else {
                      ball.setAttribute("src", mapbuttonlock);
                      link.setAttribute("href","#");
                  }
                  ball.setAttribute("style", "width:" + (imageScale * buttonwidth) + "px");
                  ball.setAttribute("title", activities[i].desc);
                  var point = path.getPointAtLength(step * i);
                  var pointx = imageSvgScale * (point.x - buttonwidth / 2);
                  var pointy = imageSvgScale * (point.y - buttonwidth / 2);
                  link.setAttribute("style", "position:absolute;top:" + pointy + "px;left:" + pointx + "px;");
                  link.appendChild(ball);
                  container.appendChild(link);

              };
              (function(img, path, nbActivities){window.addEventListener("resize", function(){
                imageScale = img.width / imagewidth;
                var step = path.getTotalLength();
                if (nbActivities > 1) {
                  step = path.getTotalLength() / (nbActivities - 1);
                }
                for (var i = 0; i < nbActivities;i++) {
                  var link = document.getElementById("link_"+ gameid + i)
                  var point = path.getPointAtLength(step * i);
                  var pointx = imageSvgScale * (point.x - 50 / 2);
                  var pointy = imageSvgScale * (point.y - 50 / 2);
                  link.setAttribute("style", "position:absolute;top:" + pointy + "px;left:" + pointx + "px;");
                }
              })}(img, path, nbActivities))
          }
          else {
            console.log("Erreur pendant le chargement de la page.");
          }
      }
  };
  xhReq.send(null);
}
