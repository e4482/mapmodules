<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Ajax script to update the contents of the question bank dialogue.
 *
 * @package     mod_mapmodules
 * @author  	Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
 * @author  	Nicolas Daugas
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('AJAX_SCRIPT', true);
global $DB, $USER;

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/mod/mapmodules/locallib.php');
require_once($CFG->dirroot . '/mod/mapmodules/lib.php');

$mapid = required_param('mapid', PARAM_INT);
$iskhan = required_param('iskhan', PARAM_BOOL);
$mapmodule     = $DB->get_record('mapmodules', array('id' => $mapid), '*', MUST_EXIST);
$course         = $DB->get_record('course', array('id' => $mapmodule->course), '*', MUST_EXIST);
$cm             = get_coursemodule_from_instance('mapmodules', $mapmodule->id, $course->id, false, MUST_EXIST);
$context        = context_module::instance($cm->id);

//require_sesskey($sesskey);
require_login($course, false, $cm);

if ($iskhan) {
    $contents = mapmodules_get_html_modules($mapmodule);
    echo json_encode(array(
        'status'   => 'OK',
        'contents' => $contents,
    ));

}
else {
    $contents = mapmodules_get_json_modules($mapmodule);
    echo json_encode($contents);
}
