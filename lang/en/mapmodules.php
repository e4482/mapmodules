<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * English strings for mapmodules
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_mapmodules
 * @author     2015 Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
 * @author     Nicolas Daugas
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['modulename'] = 'Carte de progression';
$string['modulenameplural'] = 'Cartes de progression';
$string['modulename_help'] = "<h2>Carte de progression</h2>"
	. 'La carte de progression permet à chaque élève de visualiser l\'état de son avancement dans le parcours. Chaque activité est affichée sur la carte en utilisant l`un des trois états suivants:<br><ul><li>activité non disponible</li><li>activité disponible mais inachevée</li><li>activité achevée</li></ul>';
$string['mapmodulesfieldset'] = 'Custom example fieldset';
$string['mapmodulesname'] = 'mapmodules name';
$string['mapmodulesname_help'] = 'Titre de la carte de progression.';
$string['mapmodules'] = 'mapmodules2';
$string['pluginadministration'] = 'mapmodules administration';
$string['pluginname'] = 'mapmodules3';
$string['mapmodules:addinstance'] = 'add instance mapmodules';
$string['mapmodules:view'] = 'view mapmodules';


$string['loadstandardimage'] = 'Utiliser une carte standard';

$string['mapeditorimagefilelabel'] = 'Choisir une image';
$string['iconsetlabel'] = "Jeu d'icônes des activités";
$string['buttonwidthlabel'] = "Taille des icônes";
$string['buttonwidthnaught'] = "Pas d'icône";
$string['buttonwidthtiny'] = 'Minuscule';
$string['buttonwidthverysmall'] = 'Très petite';
$string['buttonwidthsmall'] = 'Petite';
$string['buttonwidthmedium'] = 'Moyenne';
$string['buttonwidthlarge'] = 'Grande';
$string['buttonwidthextralarge'] = 'Très grande';
$string['buttonwidthgigantic'] = 'Gigantesque';
$string['displaymodulenameslabel'] = "Utiliser le mode de carte spécifique Khan";
$string['targetsectionlabel'] = "Utiliser le mode de carte spécifique Khan";


$string['mapvalidation'] = 'Aucune image sélectionnée';
$string['invalidpathvalidation'] = 'Le chemin ne comporte pas assez de points';

$string['descriptionpartlabel'] = 'Eléments supplémentaires de description à afficher';
$string['descriptionheaderlabel'] = 'Ajouter un titre ou une description en haut de la carte';
$string['descriptionfooterlabel'] = 'Ajouter un titre ou une description en bas de la carte';
